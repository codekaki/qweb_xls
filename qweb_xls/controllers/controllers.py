# -*- coding: utf-8 -*-


from openerp.addons.web.http import route, request, Controller
import json


class ReportController(Controller):
    @route([
        '/report/xls/<reportname>',
        '/report/xls/<reportname>/<docids>',
    ], type='http', auth='user', website=True)
    def report_routes(self, reportname, docids=None, **data):
        report_obj = request.registry['report']
        cr, uid, context = request.cr, request.uid, request.context
        report = report_obj._get_xls_report_from_name(cr, uid, reportname)

        if docids:
            docids = [int(i) for i in docids.split(',')]
        if data.get('options'):
            data.update(json.loads(data.pop('options')))
        if data.get('context'):
            # Ignore 'lang' here, because the context in data is the one from the webclient *but* if
            # the user explicitely wants to change the lang, this mechanism overwrites it.
            data['context'] = json.loads(data['context'])
            if data['context'].get('lang'):
                del data['context']['lang']
            context.update(data['context'])

        filename = report.name
        xls = report_obj.get_xls(cr, uid, docids, reportname, data=data, context=context)
        pdfhttpheaders = [('Content-Type', 'application/vnd.ms-excel'), ('Content-Disposition', 'attachment; filename="{}.xls"'.format(filename)), ('Content-Length', len(xls))]
        return request.make_response(xls, headers=pdfhttpheaders)
