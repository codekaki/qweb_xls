# -*- coding: utf-8 -*-
{
    'name': "Qweb XLS",

    'summary': """
        Qweb XLS
    """,

    'description': """
        This module adds Excel (.xls) format support to Qweb reporting
        engine.
    """,

    'author': "Chang Phui-Hock",
    'website': "http://www.coderii.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Technical Settings',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'report'],

    # always loaded
    'data': [
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}