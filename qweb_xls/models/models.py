# -*- coding: utf-8 -*-

from openerp import models
from functools import partial
from openerp import SUPERUSER_ID
from openerp.sql_db import TestCursor
from openerp.tools import config
from StringIO import StringIO
import lxml.html
import re
import xlwt

from . import arial10


class Report(models.Model):
    _inherit = 'report'

    def _get_xls_report_from_name(self, cr, uid, report_name):
        report_obj = self.pool['ir.actions.report.xml']
        conditions = [('report_type', '=', 'controller'), ('report_name', '=', report_name)]
        idreport = report_obj.search(cr, uid, conditions)[0]
        return report_obj.browse(cr, uid, idreport)

    def get_xls(self, cr, uid, ids, report_name, html=None, data=None, context=None):
        if context is None:
            context = {}

        if not config['test_enable']:
            context = dict(context, commit_assetsbundle=True)

        if html is None:
            html = self.get_html(cr, uid, ids, report_name, data=data, context=context)

        # The test cursor prevents the use of another environnment while the current
        # transaction is not finished, leading to a deadlock when the report requests
        # an asset bundle during the execution of test scenarios. In this case, return
        # the html version.
        if isinstance(cr, TestCursor):
            return html

        html = html.decode('utf-8')  # Ensure the current document is utf-8 encoded.

        # Get the ir.actions.report.xml record we are working on.
        report = self._get_xls_report_from_name(cr, uid, report_name)

        # Check if we have to save the report or if we have to get one from the db.
        save_in_attachment = self._check_attachment_use(cr, uid, ids, report)

        # Preparing the minimal html pages
        headerhtml = []
        footerhtml = []
        irconfig_obj = self.pool['ir.config_parameter']
        base_url = irconfig_obj.get_param(cr, SUPERUSER_ID, 'report.url') or irconfig_obj.get_param( cr, SUPERUSER_ID, 'web.base.url')

        # Minimal page renderer
        view_obj = self.pool['ir.ui.view']
        render_minimal = partial(
            view_obj.render, cr, uid, 'report.minimal_layout', context=context)

        # The received html report must be simplified. We convert it in a xml tree
        # in order to extract headers, bodies and footers.
        wb = xlwt.Workbook()
        ws = wb.add_sheet(report.name)

        try:
            root = lxml.html.fromstring(html)
            match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"

            for node in root.xpath(match_klass.format('header')):
                body = lxml.html.tostring(node)
                header = render_minimal(
                    dict(subst=True, body=body, base_url=base_url))
                headerhtml.append(header)

            for node in root.xpath(match_klass.format('footer')):
                body = lxml.html.tostring(node)
                footer = render_minimal(
                    dict(subst=True, body=body, base_url=base_url))
                footerhtml.append(footer)

            for node in root.xpath(match_klass.format('page')):
                if ids and len(ids) == 1:
                    reportid = ids[0]
                else:
                    oemodelnode = node.find(".//*[@data-oe-model='%s']" % report.model)
                    if oemodelnode is not None:
                        reportid = oemodelnode.get('data-oe-id')
                        if reportid:
                            reportid = int(reportid)
                    else:
                        reportid = False

                widths = {}
                for rowindex, tr in enumerate(node.findall(".//table[@class='xls']/tr")):
                    coloffset = 0
                    height = 0
                    for td in tr.findall("./td"):
                        content = "\n".join([text.strip() for text in td.itertext() if text.strip()])
                        content = "".join(re.split(r"\n{2,}", content))
                        ws.write(rowindex, coloffset, content)

                        widths[coloffset] = max([arial10.fitwidth(content), widths.get(coloffset, 0)])
                        height = max([arial10.fitheight(content), height])

                        colspan = int(td.get("colspan", '1'))
                        if colspan > 1:
                            ws.merge(rowindex, rowindex, coloffset, coloffset + (colspan - 1))
                        coloffset += colspan
                    ws.row(rowindex).height = int(height)

                for col, w in widths.items():
                    ws.col(col).width = int(w)

        except lxml.etree.XMLSyntaxError:
            save_in_attachment = {}  # Don't save this potentially malformed document

        output = StringIO()
        wb.save(output)
        content = output.getvalue()
        output.close()
        return content